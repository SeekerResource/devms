<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<title>开发管理</title>
<meta name="description" content="" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<%@include file="/view/common/head.jsp"%>
</head>
<body>
	<%@ include file="/view/common/navbar.jsp"%>
	<div class="main-container-inner">
		<%@ include file="/view/common/sidebar.jsp"%>

		<div class="main-content">
			<%@ include file="/view/common/search.jsp"%>

			<div class="page-content">
				<div class="row">
					<div class="col-sm-6">
						<form class="form-horizontal" id="form" method="get" action="<%=path%>/manager/dev/list">
							<p class="form-p">

								变量名&nbsp;&nbsp;<input type="text" name="variable" value="${dev.variable}" class="input-sm">&nbsp;&nbsp;&nbsp;&nbsp;
								<button onclick="javascript:doSubmit(1,'<%=path%>/manager/dev/list')" class="btn btn-sm btn-purple">
									查找 <i class="icon-search icon-on-right bigger-110"></i>
								</button>
								<button type="button" onclick="location.href='<%=path%>/manager/dev/mod'" class="btn btn-sm btn-purple">
									添加变量 <i class="icon-plus-sign icon-on-right bigger-110"></i>
								</button>
								<input type="hidden" name="pageNo" id="pageNo" value="${pageNo}"> <input type="hidden" name="length" value="${length}">
							</p>
						</form>
					</div>
					<div class="table-responsive">
						<table id="sample-table-1" class="table table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th>变量名</th>
									<th>描述</th>
									<th>备注</th>
									<th>操作</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${page.content}" var="dev" varStatus="i">
									<tr>
										<td>${i.index+(pageNo-1)*page.size+1}</td>
										<td>${dev.variable}</td>
										<td>${dev.meaning}</td>
										<td>${dev.remark}</td>
										<td>
											<button class="btn btn-xs btn-primary" onclick="location.href='<%=path %>/manager/dev/mod?id=${dev.id}'">
												<i class=" icon-edit"></i>&nbsp;编辑
											</button> 
											<button class="btn btn-xs btn-primary" onclick="if(window.confirm('确认删除？')==true)location.href='<%=path %>/manager/dev/delete?id=${dev.id}'">
												<i class="icon-remove"></i>&nbsp;删除
											</button>
											
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
					<%@ include file="/view/common/pagination.jsp"%>
				</div>
			</div>
		</div>
	</div>
	<%@ include file="/view/common/foot.jsp"%>
</body>
</html>