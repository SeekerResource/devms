package org.sr.eframe.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.sr.eframe.model.Dev;
import org.sr.eframe.repository.DevRepository;
import org.sr.eframe.service.DevService;
import org.sr.eframe.util.EframeUtil;

/**
 * @Organ: Inspur Group
 * @Teams: Big Data Team
 * @Author: zhouzhd {2014年4月28日 下午2:50:17}
 * @Mail: zzd338@163.com
 * 
 * @ClassName: UserServiceImpl
 * @Description:
 * 
 * 
 */
@Service
public class DevServiceImpl implements DevService {

	@Resource
	private DevRepository devRepository;

	public boolean saveDev(Dev dev, String login) {

		if (this.devRepository.findByVariable(dev.getVariable()) == null) {
			dev.setCreateTime(new Date());
			dev.setCreator(login);
			this.devRepository.save(dev);
			return true;
		}
		return false;
	}

	public void delete(Dev dev) {

		this.devRepository.delete(dev.getId());
	}

	public Dev update(Dev dev, String login) {

		dev.setEditTime(new Date());
		dev.setEditor(login);
		return this.devRepository.save((Dev) EframeUtil.getUpdateObject(dev, this.findOne(dev)));
	}

	public List<Dev> findAll() {

		return this.devRepository.findAll();
	}

	public Page<Dev> findAll(Integer pageNo) {

		return this.devRepository.findAll(new PageRequest(pageNo, EframeUtil.pageSize));
	}

	public Dev findOne(Dev dev) {

		return this.devRepository.findOne(dev.getId());
	}

	public Page<Dev> findByVariableLike(String variable, Integer pageNo) {

		return this.devRepository.findByVariableContaining(variable, new PageRequest(pageNo, EframeUtil.pageSize));
	}

	@Override
	public void save(Dev t, String login) {
		// TODO Auto-generated method stub
		
	}

	

}
