package org.sr.eframe.service;

import org.springframework.data.domain.Page;
import org.sr.eframe.model.Dev;

/**
 * @Organ: Inspur Group
 * @Teams: Big Data Team
 * @Author: zhouzhd {2014年4月28日 下午2:44:06}
 * @Mail: zzd338@163.com
 * 
 * @ClassName: DevService
 * @Description:
 * 
 * 
 */

public interface DevService extends BaseService<Dev> {

	public Page<Dev> findByVariableLike(String variable, Integer pageNo);

	public boolean saveDev(Dev dev, String login);
}
