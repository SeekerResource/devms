package org.sr.eframe.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @Organ: Inspur Group
 * @Teams: Big Data Team
 * @Author: xzd {2014年9月1日 上午11:17:}
 * @Mail: lyyzgyyb@126.com
 * 
 * @ClassName: Dev
 * @Description:
 * 
 * 
 */
@Entity
@Table(name = "sys_dev")
public class Dev {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;

	@Column(name = "variable")
	private String variable;

	@Column(name = "meaning")
	private String meaning;

	@Column(name = "remark")
	private String remark;

	@Column(name = "createTime")
	private Date createTime;

	@Column(name = "creator")
	private String creator;

	@Column(name = "editTime")
	private Date editTime;

	@Column(name = "editor")
	private String editor;

	public Dev() {
	}

	public Dev(String variable, String meaning, String remark) {
		this.variable = variable;
		this.meaning = meaning;
		this.remark = remark;
	}

	public Dev(Integer id) {
		this.id = id;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public Date getEditTime() {
		return editTime;
	}

	public void setEditTime(Date editTime) {
		this.editTime = editTime;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public String getVariable() {
		return variable;
	}

	public void setVariable(String variable) {
		this.variable = variable;
	}

	public String getMeaning() {
		return meaning;
	}

	public void setMeaning(String meaning) {
		this.meaning = meaning;
	}

}
