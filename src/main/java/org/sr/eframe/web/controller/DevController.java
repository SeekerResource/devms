package org.sr.eframe.web.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.sr.eframe.model.Dev;
import org.sr.eframe.service.DevService;
import org.sr.eframe.util.EframeUtil;

/**
 * @Organ: Inspur Group
 * @Teams: Big Data Team
 * @Author: zhouzhd {2014年4月28日 下午2:54:03}
 * @Mail: zzd338@163.com
 * 
 * @ClassName: UserController
 * @Description:
 * 
 * 
 */
@Controller
@RequestMapping(value = "/manager/dev")
public class DevController {

	@Resource
	private DevService devService;

	@RequestMapping(value = "/mod/dev")
	public ModelAndView mod(Dev dev, HttpSession session) {

		String login = session.getAttribute("user").toString();
		if (dev.getId() != null) {
			dev = this.devService.update(dev, login);
		} else {
			if (!this.devService.saveDev(dev, login)) {
				return new ModelAndView("dev/mod").addObject("message", "变量已存在，请更正");
			}
		}
		return new ModelAndView("redirect:/manager/dev/list");
	}

	@RequestMapping(value = "/delete")
	public ModelAndView delete(Integer pageNo, Dev dev) {

		this.devService.delete(dev);
		return new ModelAndView("redirect:/manager/dev/list");
	}

	@RequestMapping(value = "/list")
	public ModelAndView listAll(Integer pageNo, Dev dev) {

		ModelAndView mav = new ModelAndView("dev/list");
		Page<Dev> page = null;
		if (!EframeUtil.isNull(dev.getVariable())) {
			page =devService.findByVariableLike(dev.getVariable(), EframeUtil.getPageNo(pageNo));
		} else {
			page = devService.findAll(EframeUtil.getPageNo(pageNo));
		}
		mav.addObject("dev", dev);
		EframeUtil.pageHandler(mav, page, "sy_dev", EframeUtil.getPageNo(pageNo));
		mav.addObject("first", "首页").addObject("second", "开发管理");
		return mav;
	}

	@RequestMapping(value = "/mod")
	public ModelAndView toMod(Integer id) {

		ModelAndView mv = new ModelAndView("dev/mod");
		Dev dev = null;
		if (id != null) {
			dev = this.devService.findOne(new Dev(id));
		} else {
			dev = new Dev();
		}
		mv.addObject("sidebar", "sy_dev").addObject("dev", dev).addObject("first", "首页").addObject("second", "开发管理").addObject("third", "变量修改");
		return mv;
	}

}
