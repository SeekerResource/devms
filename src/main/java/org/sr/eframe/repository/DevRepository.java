package org.sr.eframe.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import org.sr.eframe.model.Dev;

/**
 * @Organ: Inspur Group
 * @Teams: Big Data Team
 * @Author: zhouzhd {2014年4月28日 上午9:51:56}
 * @Mail: zzd338@163.com
 * 
 * @ClassName: UserRepository
 * @Description:
 * 
 * 
 */
@Transactional(readOnly = true)
public interface DevRepository extends JpaRepository<Dev, Integer> {

	/**
	 * @Author: zhouzhd {2014年4月28日 下午2:37:03}
	 * @Version：
	 * @Title: findByUsernameContaining
	 * @Description: 名称模糊查询
	 * @param username
	 * @param pageable
	 * @return
	 */
	Page<Dev> findByVariableContaining(String variable, Pageable pageable);

	/**
	 * @Author: zhouzhd {2014年4月28日 下午2:37:20}
	 * @Version：
	 * @Title: findByUsernameAndPasswd
	 * @Description: 根据变量名查询
	 * @param variable
	 * @return
	 */
	

	Dev findByVariable(String variable);
}